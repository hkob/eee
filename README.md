# 電気電子工学コースオフィシャル実験実習レポートスタイルファイルプロジェクト #

このプロジェクトは電気電子工学コースのオフィシャル実験実習レポートスタイルファイルを作成することを目的とします.

### eee.sty とは ###

* 電気電子工学コースの学生が LaTeX でレポートを記述するためのスタイルファイルである。
* Version 0.1

### 使い方 ###

* 中に入っている template.tex をコンパイルできることを確認してください
* 具体的な使用方法は、コンパイルしてできる template.pdf を参照してください。
* report.tex は最低限の内容が記述されたファイルです。新規のレポートはここからスタートしてください。

### このプロジェクトに貢献するためには ###

* こんな表記がしたいという意見をいただきたい
* 便利なマクロがあったら紹介して欲しい
